<?php
namespace Datum\Square\Model;
class Like extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'datum_square_like';

    protected $_cacheTag = 'datum_square_like';

    protected $_eventPrefix = 'datum_square_like';

    protected function _construct()
    {
        $this->_init('Datum\Square\Model\ResourceModel\Like');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
