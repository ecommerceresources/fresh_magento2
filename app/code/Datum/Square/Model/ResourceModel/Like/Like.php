<?php
namespace Datum\Square\Model\ResourceModel;


class Like extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('datum_square_like', 'like_id');
    }

}