<?php
namespace Datum\Square\Model\ResourceModel\Like;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'like_id';
    protected $_eventPrefix = 'datum_square_like_collection';
    protected $_eventObject = 'like_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Datum\Square\Model\Like', 'Datum\Square\Model\ResourceModel\Like');
    }
}