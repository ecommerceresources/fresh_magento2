define([
    'jquery',
    'underscore',
    'mage/template'
], function (
    $,
    _,
    template
) {
    function main(config, element) {
        var $element = $(element);
        var YOUR_URL_HERE = config.AjaxUrl;
        var formData = config.formData;
        var numberValue = $('#number').val();
        // var numberValuee = $('input[name="number"]').val();
        var numberValuee = $('#clickAjax').text();
        $(document).on('click','#clickAjax',function() {
            console.log('ajax form checking');
            console.log(numberValue);
            console.log(numberValuee);
            var param = {form_data: formData, ajax:1, numberValue:numberValuee};
            $.ajax({
                showLoader: true,
                url: YOUR_URL_HERE,
                data: param,
                type: "POST",
                dataType: 'json'
            }).done(function (data) {
                $('#formtest').removeClass('hideme');
                var html = template('#formtest', {posts:data});
                $('#formtest').html(html);
            });
        });
    };
    return main;
});