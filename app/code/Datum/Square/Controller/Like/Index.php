<?php
namespace Datum\Square\Controller\Like;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Datum\Square\Model\LikeFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;

    protected $_likeFactory;

    protected  $resultJsonFactory;

    public function __construct(
            Context $context,
            PageFactory $pageFactory,
            LikeFactory $likeFactory,
            \Magento\Framework\Controller\Result\JsonFactory    $resultJsonFactory
            )
    {
        $this->pageFactory = $pageFactory;
        $this->_likeFactory = $likeFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    /**
     * Single controller method to fetch and post data from view
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $product_id = $this->getRequest()->getParam('product_id');
        $status = $this->getRequest()->getParam('status');
        $customer_id = $this->_objectManager->create('Webkul\Marketplace\Helper\Data')->getCustomerId();
        $like = $this->_likeFactory->create();
        if ($status == 'dislike') {
            $collection = $like->getCollection()
                ->addFieldToFilter('product_id', array('eq' => $product_id))
                ->addFieldToFilter('customer_id', array('eq' => $customer_id));
           foreach($collection as $item) {
               $item->delete();
           }
        } else if ($status = 'like') {
            $like->setProductId($product_id);
            $like->setCustomerId($customer_id);
            $like->save();
        }
        
    }
}