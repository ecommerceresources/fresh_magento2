<?php
namespace First\HelloWorld\Model;
class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'first_helloworld_post';

    protected $_cacheTag = 'first_helloworld_post';

    protected $_eventPrefix = 'first_helloworld_post';

    protected function _construct()
    {
        $this->_init('First\HelloWorld\Model\ResourceModel\Post');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
