<?php
namespace First\HelloWorld\Controller\Hello;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class Add extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;

    protected $_postFactory;

    public function __construct(
            Context $context,
            PageFactory $pageFactory,
            \First\HelloWorld\Model\PostFactory $postFactory
            )
    {
        $this->pageFactory = $pageFactory;
        $this->_postFactory = $postFactory;
        return parent::__construct($context);
    }

    /**
     * Single controller method to fetch and post data from view
     */
    public function execute()
    {
        $number = $this->getRequest()->getParam('number');

        $page_object = $this->pageFactory->create();
        return $page_object;
    }
}