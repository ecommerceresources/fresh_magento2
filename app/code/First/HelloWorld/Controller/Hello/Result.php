<?php
namespace First\HelloWorld\Controller\Hello;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use First\HelloWorld\Model\PostFactory;

class Result extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;

    protected $_postFactory;

    protected  $resultJsonFactory;

    public function __construct(
            Context $context,
            PageFactory $pageFactory,
            PostFactory $postFactory,
            \Magento\Framework\Controller\Result\JsonFactory    $resultJsonFactory
            )
    {
        $this->pageFactory = $pageFactory;
        $this->_postFactory = $postFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    /**
     * Single controller method to fetch and post data from view
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax())
        {
            $number = $this->getRequest()->getParam('formData');
            $numberValue = $this->getRequest()->getParam('numberValue');
            $test = Array
            (
                'formdata' => $numberValue,
                'Firstname' => 'What is your firstname',
                'Email' => 'What is your emailId',
                'Lastname' => 'What is your lastname',
                'Country' => 'Your Country'
            );
            return $result->setData($test);
        }
    }
}