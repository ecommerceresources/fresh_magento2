<?php
namespace First\HelloWorld\Controller\Hello;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;

    protected $_postFactory;

    public function __construct(
            Context $context,
            PageFactory $pageFactory,
            \First\HelloWorld\Model\PostFactory $postFactory
            )
    {
        $this->pageFactory = $pageFactory;
        $this->_postFactory = $postFactory;
        return parent::__construct($context);
    }

    /**
     * Single controller method to fetch and post data from view
     */
    public function execute()
    {
        // Helper call in controller
        $helperData = $this->_objectManager->create('First\HelloWorld\Helper\Data')->RandomFunc();

        // Object of custom model to fetch data from model
        $post = $this->_postFactory->create();
        /** @var TYPE_NAME $collection */
        $collection = $post->getCollection()->addFieldToFilter('post_id', array('eq' => 2));
//        foreach($collection as $item){
//            echo "<pre>";
//            print_r($item->getData());
//            echo "</pre>";
//        }
        //exit();
        $page_object = $this->pageFactory->create();
        return $page_object;
    }
}